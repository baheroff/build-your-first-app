package com.example.diceroller

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<Button>(R.id.roll_button).apply {
            setOnClickListener {
                rollDice()
            }
        }

        findViewById<Button>(R.id.count_button).apply {
            setOnClickListener {
                countUp()
            }
        }
    }

    private fun rollDice() {
        // Toast.makeText(this, "Button Clicked", Toast.LENGTH_SHORT).show()

        findViewById<TextView>(R.id.result_text).apply {
            // this.text = getString(R.string.rolled)
            this.text = (1..6).random().toString()
        }
    }

    private fun countUp() {

        findViewById<TextView>(R.id.result_text).apply {
            this.text =
                when (this.text.toString()) {
                    "6" -> return
                    "Hello World!" -> "1"
                    else -> this.text.toString().toInt().inc().toString()
                }
        }
    }

}